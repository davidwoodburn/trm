# CHANGELOG

## 1.4.15 (2025-01-07)

### Fixed

*   Plotting flat profiles of negative values.

## 1.4.14 (2024-12-04)

### Fixed

*   Poor robustness in plotting flat profiles.

## 1.4.13 (2024-12-04)

### Fixed

*   File name is now optional.

### New

*   Added command-line support for heat and spy.

## 1.4.12 (2024-12-04)

### Fixed

*   iplot of flat y data which rendered as nans.

### New

*   Command-line call of `colors` function.

## 1.4.11 (2024-12-02)

### Fixed

*   The printing of numbers by table within a fixed width.
*   Tables wider than the terminal window.

### New

*   Added csv and LaTeX formatted tables.

## 1.4.10 (2024-11-27)

### Fixed

*   Width of values printed by table() function.

### New

*   Added the colors() function.

## 1.4.9 (2024-11-19)

### New

*   Added JOSS badge.

## 1.4.8 (2024-11-18)

### Fixed

*   Corrected author list for gnuplot in paper.

## 1.4.7 (2024-11-13)

### Fixed

*   Added distribution statement to LICENSE.

## 1.4.6 (2024-11-13)

### Fixed

*   Wrong variable name in zoom_in_indices.

## 1.4.5 (2024-11-06)

### Fixed

*   Removed uniqueness statement from paper.

## 1.4.4 (2024-11-06)

### New

*   Ability to call itrm.iplot directly from the command line.

## 1.4.3 (2024-11-06)

### Fixed

*   Incorrect and missing documentation.

## 1.4.2 (2024-11-06)

### Fixed

*   Removed diagnostic print.

## 1.4.1 (2024-11-06)

### Fixed

*   Added installation instructions to README.

## 1.4.0 (2024-11-06)

### New

*   Changed configuration file location to more standard locations.
*   Added ability to define configuration file location with environment
    variable.
*   Added ability to override configuration settings directly in script.
*   Added images in the figures folder showing expected results from examples.

## 1.3.3 (2024-11-04)

### Fixed

*   Added dependencies to paper.

## 1.3.2 (2024-11-04)

### Fixed

*   Problems with JOSS paper.
*   Missing explanations in example scripts.
*   Removed reference to `itrm.CONFIG.ar` in eg_circle.py.
*   Removed colons at end of iplot and plot examples in README.
*   Incorrect fault for heat and spy plots in ASCII mode for undersized terminal
    windows.
*   Changed `raise ValueError` to `print` in heat and spy for undersized
    terminal windows so other code can continue.

### New

*   Squeeze heat and spy plots in ASCII mode to help make them fit in undersized
    terminal windows.

## 1.3.1 (2024-10-10)

### Fixed

*   Missing code update.

## 1.3.0 (2024-10-09)

### New

*   Added Allan variances.
*   Changed some of the keybindings to make room for more.

### Fixed

*   Occasions where the range of y values under the cursor returned `nan` when
    there should be a finite range.
*   Situations where the some points no longer plot when zooming in.

## 1.2.1 (2024-10-02)

### New

*   Added draft Journal of Open Source Software paper.

## 1.2.0 (2024-09-11)

### Fixed

*   Calculation of significant figures in sigfigs.

### New

*   Show only unique points with the `fu` command.

## 1.1.2 (2024-09-09)

### Fixed

*   Corrected default value in README.md file.
*   Added padding to table figure.

## 1.1.0 (2024-09-09)

### New

*   The config function was removed in favor of directly modifying the
    config.ini file.
*   The plot and iplot function now have an overriding cmap parameter.
*   The information bar at the bottom of plot and iplot functions now
    automatically collapses depending on the available space. The information
    set can by cycled by pressing the 'm' or 'M' keys.

## 1.0.19 (2024-??-??)

### Fixed

*   The bars function can now handle 0 entries.

### New

*   Added numbers at the ends of bars.

## 1.0.18 (2024-08-19)

### Fixed

*   Removed metadata from `itrm.py` in favor of `pyproject.toml` metadata.

## 1.0.17 (2024-08-05)

### Fixed

*   Removed error for insufficient labels.

## 1.0.16 (2024-07-31)

### Fixed

*   Do not add number labels for single data sets.

## 1.0.15 (2024-07-31)

### Fixed

*   Adding numeric labels correctly.

## 1.0.14 (2024-07-30)

### New

*   Added numeric labels when no labels are provided.

## 1.0.13 (2024-07-19)

### Fixed

*   Plot the full view of non-monotonic data
*   Progress bar works correctly when escape sequences are not available

## 1.0.12 (2024-07-16)

### Fixed

*   Added compatibility with Tabby on Windows. Alt-arrow keys map to ctrl-arrow
    keys. This is a known and unresolved defect with Tabby:
    https://github.com/Eugeny/tabby/issues/2328.

## 1.0.11 (2024-07-15)

### Docs

*   Added note about alt-arrow keys on Windows

## 1.0.10 (2024-07-15)

### Fixed

*   Posix support for quit on delete key
*   Windows support for quit on delete, backspace, and enter
*   Windows support for alt-arrow keys

## 1.0.9 (2024-07-13)

### New

*   Made the selected curve plot last so it would be on top and visible

## 1.0.8 (2024-07-12)

### New

*   Changed trim to be an f function

## 1.0.7 (2024-07-11)

### Fixed

*   Ensured step size would not be smaller than EPS

## 1.0.6 (2024-07-11)

### Fixed

*   Fixed cursor metrics when plotting a single data set

## 1.0.5 (2024-07-11)

### Fixed

*   Improved cursor sig figs

## 1.0.4 (2024-07-11)

### Fixed

*   Fixed exit command during normal quit

## 1.0.3 (2024-07-10)

### Updated

*   Changed term attributes to class

## 1.0.2 (2024-07-10)

### Fixed

*   Fixed lack of limit to color indexing

## 1.0.1 (2024-07-9)

### New

*   Added trim

## 1.0.0 (2024-07-9)

### Updated

*   Major rework
